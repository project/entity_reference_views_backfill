<?php

/**
 * @file
 * Hooks provided by the Entity Reference Views Backfill module.
 */

use Drupal\paragraphs\ParagraphInterface;

/**
 * Alters the compiled views parameters before the view is ran.
 *
 * @param array $parameters
 *   The compiled parameters to be passed to the view.
 * @param array $context
 *   An array of contextual information to be used for further parameter
 *   computation.
 */
function hook_entity_reference_views_backfill_parameters_alter(array &$parameters, array &$context) {
  /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
  $items = $context['items'];
  /** @var \Drupal\paragraphs\ParagraphInterface $parent */
  $parent = $items->getParent()->getEntity();

  // Only enable backfill if one of the parameters is set.
  $enabled = FALSE;
  if ($parent->hasField('field_content_types') && !$parent->get('field_content_types')->isEmpty()) {
    $enabled = TRUE;
  }
  if ($parent->hasField('field_taxonomy_categories') && !$parent->get('field_taxonomy_categories')->isEmpty()) {
    $enabled = TRUE;
  }
  if ($parent->hasField('field_taxonomy_tags') && !$parent->get('field_taxonomy_tags')->isEmpty()) {
    $enabled = TRUE;
  }
  $parameters['enabled'] = $enabled;
}

/**
 * Alters the compiled views parameters before the view is ran.
 *
 * @param array $parameters
 *   The compiled parameters to be passed to the view.
 * @param array $context
 *   An array of contextual information to be used for further parameter
 *   computation.
 */
function hook_entity_reference_views_backfill_parameters_PARENT_ENTITY_TYPE_FIELD_NAME_alter(array &$parameters, array &$context) {
  /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
  $items = $context['items'];

  // Get node IDs already referenced by other paragraphs on this paragraphs
  // field to make sure they aren't pulled in automatically.
  foreach ($items as $item) {
    if ($item->entity instanceof ParagraphInterface) {
      if ($item->entity->bundle() === 'referenced_content') {
        $selected_node_ids[] = $item->entity->get('field_node')->target_id;
      }
    }
  }
  $parameters['argument'][] = implode('+', $selected_node_ids);
}
