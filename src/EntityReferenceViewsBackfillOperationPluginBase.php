<?php

namespace Drupal\entity_reference_views_backfill;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for entity_reference_views_backfill_operation plugins.
 */
abstract class EntityReferenceViewsBackfillOperationPluginBase extends PluginBase implements EntityReferenceViewsBackfillOperationInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function description() {
    return isset($this->pluginDefinition['description']) ? $this->pluginDefinition['description'] : '';
  }

  /**
   * {@inheritdoc}
   */
  public function fieldLabel() {
    return isset($this->pluginDefinition['field_label']) ? $this->pluginDefinition['field_label'] : '';
  }

  /**
   * {@inheritdoc}
   */
  public function fieldDescription() {
    return isset($this->pluginDefinition['field_description']) ? $this->pluginDefinition['field_description'] : '';
  }

  /**
   * {@inheritdoc}
   */
  public function fieldRenderArray($default_value, array $context) {
    return [
      '#type' => 'textfield',
      '#default_value' => $default_value,
      '#title' => $this->fieldLabel() ?: $this->label(),
      '#description' => $this->fieldDescription(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function processReplacement($value, array $context) {
    return NULL;
  }

}
