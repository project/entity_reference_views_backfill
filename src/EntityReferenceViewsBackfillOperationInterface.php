<?php

namespace Drupal\entity_reference_views_backfill;

/**
 * Interface for entity_reference_views_backfill_operation plugins.
 */
interface EntityReferenceViewsBackfillOperationInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * Returns the translated plugin description.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The translated description.
   */
  public function description();

  /**
   * Returns the translated plugin fields label.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The translated field label.
   */
  public function fieldLabel();

  /**
   * Returns the translated plugin field description.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The translated field description.
   */
  public function fieldDescription();

  /**
   * Returns the plugins field array for processing values.
   *
   * @param mixed $default_value
   *   The default values for the field.
   * @param array $context
   *   An array of contextual information.
   *
   * @return array
   *   The field render array.
   */
  public function fieldRenderArray($default_value, array $context);

  /**
   * Returns a replacement for the specified value.
   *
   * @param string $value
   *   The value to be processed and replaced or returned.
   * @param array $context
   *   An array of contextual information.
   *
   * @return string|int|null
   *   The replacement value, otherwise null.
   */
  public function processReplacement($value, array $context);

}
