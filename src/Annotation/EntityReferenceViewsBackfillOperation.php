<?php

namespace Drupal\entity_reference_views_backfill\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines entity_reference_views_backfill_operation annotation object.
 *
 * @Annotation
 */
class EntityReferenceViewsBackfillOperation extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translationoptional
   *
   * @ingroup plugin_translatable
   */
  public $description = '';

  /**
   * The field label for the plugin value.
   *
   * @var \Drupal\Core\Annotation\Translationoptional
   *
   * @ingroup plugin_translatable
   */
  public $fieldLabel = '';

  /**
   * The field description for the plugin value.
   *
   * @var \Drupal\Core\Annotation\Translationoptional
   *
   * @ingroup plugin_translatable
   */
  public $fieldDescription = '';

  /**
   * The field description for the plugin value.
   *
   * @var arrayoptional
   */
  public $handlerTypes = [];

}
