<?php

namespace Drupal\entity_reference_views_backfill\Plugin\EntityReferenceViewsBackfillOperation;

use Drupal\entity_reference_views_backfill\EntityReferenceViewsBackfillOperationPluginBase;

/**
 * Plugin implementation of the entity_reference_views_backfill_operation.
 *
 * @EntityReferenceViewsBackfillOperation(
 *   id = "static",
 *   label = @Translation("Static value"),
 *   description = @Translation("Use a hard-coded static value.")
 * )
 */
class StaticOperation extends EntityReferenceViewsBackfillOperationPluginBase {

  /**
   * {@inheritdoc}
   */
  public function processReplacement($value, array $context) {
    return $value;
  }

}
