<?php

namespace Drupal\entity_reference_views_backfill\Plugin\EntityReferenceViewsBackfillOperation;

use Drupal\entity_reference_views_backfill\EntityReferenceViewsBackfillOperationPluginBase;

/**
 * Plugin implementation of the entity_reference_views_backfill_operation.
 *
 * @EntityReferenceViewsBackfillOperation(
 *   id = "default",
 *   label = @Translation("Default"),
 *   description = @Translation("Use the default value for the handler."),
 * )
 */
class DefaultOperation extends EntityReferenceViewsBackfillOperationPluginBase {

  /**
   * {@inheritdoc}
   */
  public function fieldRenderArray($default_value, array $context) {
    return NULL;
  }

}
