<?php

namespace Drupal\entity_reference_views_backfill\Plugin\EntityReferenceViewsBackfillOperation;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_reference_views_backfill\EntityReferenceViewsBackfillOperationPluginBase;

/**
 * Plugin implementation of the entity_reference_views_backfill_operation.
 *
 * @EntityReferenceViewsBackfillOperation(
 *   id = "field",
 *   label = @Translation("Sibling field from parent"),
 *   description = @Translation("Get a field value from the parent and return it."),
 *   field_label = @Translation("Field name"),
 *   field_description = @Translation("Specity a field name to pull value from that field on parent entity.")
 * )
 */
class FieldOperation extends EntityReferenceViewsBackfillOperationPluginBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function fieldRenderArray($default_value, array $context) {
    if ($context['handler_type'] == 'argument') {
      $elements = [
        '#type' => 'fieldset',
        '#title' => $this->fieldLabel() ?: $this->label(),
        '#title_display' => 'invisible',
        'value' => [
          '#type' => 'textfield',
          '#default_value' => (is_array($default_value) && isset($default_value['value'])) ? $default_value['value'] : '',
          '#title' => $this->fieldLabel() ?: $this->label(),
          '#description' => $this->fieldDescription(),
        ],
        'combine' => [
          '#type' => 'select',
          '#title' => $this->t('Combine'),
          '#description' => $this->t('What character to use to combine multiple argument values.'),
          '#default_value' => (is_array($default_value) && isset($default_value['combine'])) ? $default_value['combine'] : '',
          '#options' => [
            'comma' => $this->t(','),
            'plus' => $this->t('+'),
          ],
          '#weight' => 1,
        ],
      ];
      return $elements;
    }

    return parent::fieldRenderArray($default_value, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function processReplacement($value, array $context) {
    $parent = $context['items']->getParent()->getEntity();
    if ($parent instanceof FieldableEntityInterface && $parent->hasField($value)) {
      if (!$parent->get($value)->isEmpty()) {
        return array_column($parent->get($value)->getValue(), $parent->get($value)->first()->mainPropertyName());
      }
    }
    return NULL;
  }

}
