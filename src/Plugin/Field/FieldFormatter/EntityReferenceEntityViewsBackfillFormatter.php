<?php

namespace Drupal\entity_reference_views_backfill\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\views\ViewEntityInterface;

/**
 * Plugin implementation of the 'entity reference rendered entity' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_entity_views_backfill_view",
 *   label = @Translation("Rendered entity with Views backfill"),
 *   description = @Translation("Display the referenced entities and automatic backfill from a View rendered by entity_view()."),
 *   field_types = {
 *     "entity_reference",
 *     "entity_reference_revisions"
 *   }
 * )
 */
class EntityReferenceEntityViewsBackfillFormatter extends EntityReferenceEntityFormatter {

  /**
   * The backfill operation plugin manager.
   *
   * @var \Drupal\entity_reference_views_backfill\EntityReferenceViewsBackfillOperationPluginManager
   */
  protected $backfillManager = NULL;

  /**
   * Gets the backfill manager.
   *
   * @return \Drupal\entity_reference_views_backfill\EntityReferenceViewsBackfillOperationPluginManager
   *   The backfill plugin manager.
   */
  protected function getBackfillManager() {
    if (is_null($this->backfillManager)) {
      $this->backfillManager = \Drupal::service('plugin.manager.entity_reference_views_backfill_operation');
    }
    return $this->backfillManager;
  }

  /**
   * Gets an array of plugin instances.
   *
   * @return \Drupal\entity_reference_views_backfill\EntityReferenceViewsBackfillOperationInterface[]
   *   An array of Backfill Operation plugin instances.
   */
  protected function getBackfillOperationPluginInstances() {
    $instances = [];
    foreach ($this->getBackfillManager()->getDefinitions() as $definition_id => $definition) {
      $instances[$definition_id] = $this->getBackfillManager()->createInstance($definition_id);
    }
    return $instances;
  }

  /**
   * Returns the value of a setting, or its default value if absent.
   *
   * @param array|string $key
   *   The setting name.
   * @param mixed $default_return
   *   The default return value if setting doesn't exist.
   *
   * @return mixed
   *   The setting value.
   */
  protected function getNestedSetting($key, $default_return = NULL) {
    $key = (is_array($key)) ? $key : [$key];
    $key_exists = FALSE;
    $settings = $this->getSettings();
    $value = NestedArray::getValue($settings, $key, $key_exists);
    return ($key_exists) ? $value : $default_return;
  }

  /**
   * Get view names for a list of view machine names.
   *
   * @return array
   *   An array with view labels keyed by machine name.
   */
  protected function getViewListOptions() {
    $options = [];
    foreach ($this->entityTypeManager->getStorage('view')->loadMultiple() as $id => $view) {
      $options[$id] = $view->label();
    }
    return $options;
  }

  /**
   * Get displays for a particular view.
   *
   * @param \Drupal\views\ViewEntityInterface $view
   *   The view entity.
   *
   * @return array
   *   An array containing displays for the view.
   */
  protected function getViewDisplayListOptions(ViewEntityInterface $view) {
    $options = [];

    if ($displays = $view->get('display')) {
      foreach ($displays as $display) {
        $options[$display['id']] = $display['display_title'];
      }
    }

    return $options;
  }

  /**
   * Gets a View entity by ID.
   *
   * @param string $view_id
   *   The view entity ID.
   *
   * @return \Drupal\views\ViewEntityInterface|null
   *   The view if it exists, null otherwise.
   */
  protected function getView($view_id) {
    return (!is_null($view_id)) ? $this->entityTypeManager->getStorage('view')->load($view_id) : NULL;
  }

  /**
   * Gets a display instance from a view.
   *
   * @param \Drupal\views\ViewEntityInterface $view
   *   The view entity.
   * @param string $display_name
   *   Thew display name.
   *
   * @return \Drupal\views\Plugin\views\display\DisplayPluginInterface|null
   *   The display if it exists, null otherwise.
   */
  protected function getViewDisplay(ViewEntityInterface $view, $display_name) {
    if ($view->getExecutable()->setDisplay($display_name)) {
      return $view->getExecutable()->getDisplay();
    }

    return NULL;
  }

  /**
   * Returns the automated backfill entities for display.
   *
   * @param \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items
   *   The item list.
   * @param array $parameters
   *   The default parameters to use for the view.
   * @param string $langcode
   *   The language code of the referenced entities to display.
   * @param \Drupal\Core\Cache\CacheableMetadata $cacheable_metadata
   *   A cacheable metadata object to compile loaded entity metadata.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The array of referenced entities to display, keyed by delta.
   *
   * @see ::getEntitiesToView()
   */
  protected function getAutomatedEntitiesToView(EntityReferenceFieldItemListInterface $items, array &$parameters, $langcode, CacheableMetadata $cacheable_metadata) {
    $entities = [];

    $view_name = $this->getSetting('view_name');
    $view_display_id = $this->getSetting('display_id');
    if ($view_name && $view_entity = $this->getView($view_name)) {
      /** @var \Drupal\views\ViewExecutable $view */
      $view = $view_entity->getExecutable();
      $view->initDisplay();
      $view->setDisplay($view_display_id);
      $display = $this->getViewDisplay($view_entity, $view_display_id);
      $operations = $this->getBackfillOperationPluginInstances();
      $replacement_context = ['items' => $items];

      // Set limit value.
      $limit_operation = $this->getNestedSetting(['limit', 'operation']);
      if (array_key_exists($limit_operation, $operations)) {
        $value = $operations[$limit_operation]->processReplacement(
          $this->getNestedSetting(['limit', $limit_operation]),
          $replacement_context + ['handler_type' => 'limit']
        );
        if (!is_null($value)) {
          $parameters['limit'] = $value;
        }
      }

      // Set sort by value.
      $sort_operation = $this->getNestedSetting(
        ['exposed_input', 'sort_by', 'operation']
      );
      if (array_key_exists($sort_operation, $operations)) {
        $value = $operations[$sort_operation]->processReplacement(
          $this->getNestedSetting(
            ['exposed_input', 'sort_by', $sort_operation]
          ),
          $replacement_context + ['handler_type' => 'sort_by']
        );
        if (!is_null($value)) {
          $parameters['exposed_input']['sort_by'] = $value;
        }
      }

      // Set filter values.
      $filters = $this->getNestedSetting(['exposed_input', 'filter'], []);
      foreach ($filters as $filter_id => $filter) {
        $filter_operation = isset($filter['operation']) ? $filter['operation'] : NULL;
        /** @var \Drupal\views\Plugin\views\filter\FilterPluginBase $handler */
        if (array_key_exists($filter_operation, $operations) && $handler = $display->getHandler('filter', $filter_id)) {
          if ($handler->canExpose() && $handler->isExposed()) {
            $value = $operations[$filter_operation]->processReplacement(
              isset($filter[$filter_operation]) ? $filter[$filter_operation] : NULL,
              $replacement_context + ['handler_type' => 'filter']
            );
            if (!is_null($value)) {
              $exposed_input_key = isset($handler->options['expose']['identifier']) ? $handler->options['expose']['identifier'] : $filter_id;
              $parameters['exposed_input'][$exposed_input_key] = $value;
            }
          }
        }
      }

      // Set argument values.
      foreach ($this->getNestedSetting(['argument'], []) as $argument_id => $argument) {
        $argument_operation = isset($argument['operation']) ? $argument['operation'] : NULL;
        /** @var \Drupal\views\Plugin\views\filter\FilterPluginBase $handler */
        if (array_key_exists($argument_operation, $operations) && $handler = $display->getHandler('argument', $argument_id)) {
          $value = $operations[$argument_operation]->processReplacement(
            isset($argument[$argument_operation]) ? $argument[$argument_operation] : NULL,
            $replacement_context + ['handler_type' => 'argument']
          );
          if (!is_null($value)) {
            $parameters['argument'][$argument_id] = $value;
          }
        }
      }

      $alter_hooks = [
        'entity_reference_views_backfill_parameters',
        'entity_reference_views_backfill_parameters_' . $items->getParent()->getEntity()->getEntityTypeId() . '_' . $items->getFieldDefinition()->getName()
      ];

      $alter_context['items'] = &$items;
      $alter_context['view'] = &$view;
      $alter_context['langcode'] = $langcode;
      $alter_context['cacheable_metadata'] = &$cacheable_metadata;

      \Drupal::moduleHandler()->alter($alter_hooks, $parameters, $alter_context);
      \Drupal::theme()->alter($alter_hooks, $parameters, $alter_context);

      if ($parameters['enabled']) {
        $view->setArguments($parameters['argument']);
        $view->setExposedInput($parameters['exposed_input']);
        $view->setItemsPerPage($parameters['limit']);
        $view->preExecute();
        $view->execute();

        $entity_type_id = $view->getBaseEntityType()->id();
        $entity_ids = array_column($view->result, $view->getBaseEntityType()->getKey('id'));
        foreach ($this->entityTypeManager->getStorage($entity_type_id)->loadMultiple($entity_ids) as $entity) {
          // Set the entity in the correct language for display.
          if ($entity instanceof TranslatableInterface) {
            $entity = \Drupal::service('entity.repository')->getTranslationFromContext($entity, $langcode);
          }

          $access = $this->checkAccess($entity);
          $cacheable_metadata->addCacheableDependency(CacheableMetadata::createFromObject($access));

          if ($access->isAllowed()) {
            $entities[] = $entity;
          }
        }
        $cacheable_metadata->addCacheableDependency($view->display_handler->getCacheMetadata());
        $cacheable_metadata->addCacheTags($view->getCacheTags());
        $cacheable_metadata->addCacheContexts($view->storage->getCacheContexts());
      }
    }

    return $entities;
  }

  /**
   * Generates a field render array group for each handler.
   *
   * @param string $handler_label
   *   The label for the group.
   * @param string $handler_description
   *   The description for the group.
   * @param mixed $default_values
   *   The default values for the group to pass through to the plugins.
   * @param array $context
   *   An array of contextual information to pass through to the plugins.
   *
   * @return array
   *   A field render array.
   */
  protected function getHandlerFieldGroup($handler_label, $handler_description, $default_values, array $context) {
    $default_values = (is_array($default_values)) ? $default_values : [$default_values];

    $elements = [
      '#type' => 'fieldset',
      '#title' => $handler_label,
      '#description' => $handler_description,
    ];

    $elements['operation'] = [
      '#title' => $this->t('Operation'),
      '#title_display' => 'before',
      '#type' => 'select',
      '#options' => array_column($this->getBackfillManager()->getDefinitions(), 'label', 'id'),
      '#default_value' => NestedArray::getValue($default_values, ['operation']),
    ];

    foreach ($this->getBackfillOperationPluginInstances() as $plugin_id => $plugin) {
      if ($field_array = $plugin->fieldRenderArray(NestedArray::getValue($default_values, [$plugin_id]), $context)) {
        $field_array['#states']['visible']['select[name="fields[' . implode('][', $context['parents']) . '][operation]"]'] = ['value' => $plugin_id];
        $field_array['#states']['required'] = $field_array['#states']['visible'];
        $elements[$plugin_id] = $field_array;
      }
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'enabled' => '',
      'limit' => '',
      'view_name' => NULL,
      'display_id' => 'default',
      'view_mode_backfill' => 'default',
      'exposed_input' => [],
      'argument' => [],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $field_name = $this->fieldDefinition->getName();

    $html_wrapper_id = Html::getUniqueId(implode('-',
      ['fields', $field_name, 'settings_edit_form', 'settings']
    ));

    $elements['view_name'] = [
      '#title' => $this->t('View'),
      '#type' => 'select',
      '#options' => $this->getViewListOptions(),
      '#default_value' => $this->getSetting('view_name'),
      '#weight' => 1,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [get_class($this), 'settingsFormAjaxRefresh'],
        'event' => 'change',
        'wrapper' => $html_wrapper_id,
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading displays...'),
        ],
      ]
    ];

    // Load View if it's set.
    $view_name = NestedArray::getValue($form_state->getUserInput(),
      ['fields', $field_name, 'settings_edit_form', 'settings', 'view_name']
    );
    $view_name = $view_name ?: $this->getSetting('view_name');
    $view = $this->getView($view_name);

    // Load display options and set selected display.
    $display_options = (!is_null($view)) ? $this->getViewDisplayListOptions($view) : [];
    $display_id = NestedArray::getValue($form_state->getUserInput(),
      ['fields', $field_name, 'settings_edit_form', 'settings', 'display_id']
    );
    $display_id = $display_id ?: $this->getSetting('display_id');
    if (!in_array($display_id, array_keys($display_options))) {
      $display_id = 'default';
    }

    $elements['display_id'] = [
      '#title' => $this->t('Display'),
      '#type' => 'select',
      '#options' => $display_options,
      '#default_value' => $display_id,
      '#weight' => 2,
      '#states' => [
        'visible' => [
          'select[name="fields[' . $field_name . '][settings_edit_form][settings][view_name]"]' => ['!value' => ''],
        ],
        'required' => [
          'select[name="fields[' . $field_name . '][settings_edit_form][settings][view_name]"]' => ['!value' => ''],
        ],
      ],
      '#ajax' => [
        'callback' => [get_class($this), 'settingsFormAjaxRefresh'],
        'event' => 'change',
        'wrapper' => $html_wrapper_id,
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading filter options...'),
        ],
      ]
    ];

    $elements['view_mode_backfill'] = [
      '#type' => 'select',
      '#options' => !is_null($view) ? $this->entityDisplayRepository->getViewModeOptions($view->getExecutable()->getBaseEntityType()->id()) : [],
      '#title' => t('View mode for backfill items'),
      '#description' => $this->t('Will use default view-mode selected above if none is selected.'),
      '#default_value' => $this->getSetting('view_mode_backfill'),
      '#states' => [
        'visible' => [
          'select[name="fields[' . $field_name . '][settings_edit_form][settings][view_name]"]' => ['!value' => ''],
        ],
      ],
      '#weight' => 3,
    ];

    $field_settings_parents = [$field_name, 'settings_edit_form', 'settings'];

    if ($view && $display_id && $display = $this->getViewDisplay($view, $display_id)) {
      $enabled_context['parents'] = $field_settings_parents;
      $enabled_context['parents'][] = 'enabled';
      $enabled_context['handler_type'] = 'enabled';
      $elements['enabled'] = $this->getHandlerFieldGroup(
        $this->t('Enable backfill'),
        $this->t('Whether the backfill should be dynamically enabled or disabled.'),
        $this->getNestedSetting('enabled', []),
        $enabled_context
      ) + ['#weight' => 5];

      $limit_context['parents'] = $field_settings_parents;
      $limit_context['parents'][] = 'limit';
      $limit_context['handler_type'] = 'limit';
      $elements['limit'] = $this->getHandlerFieldGroup(
        $this->t('Limit'),
        NULL,
        $this->getNestedSetting('limit', []),
        $limit_context
      ) + ['#weight' => 5];

      $elements['exposed_input'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Exposed Input'),
        '#weight' => 5,
      ];

      $sort_by_context['parents'] = $field_settings_parents;
      $sort_by_context['parents'][] = 'exposed_input';
      $sort_by_context['parents'][] = 'sort_by';
      $sort_by_context['handler_type'] = 'sort_by';
      $elements['exposed_input']['sort_by'] = $this->getHandlerFieldGroup(
        $this->t('Sort by'),
        NULL,
        $this->getNestedSetting(['exposed_input', 'sort_by'], []),
        $sort_by_context
      );

      // Build exposed filter options.
      if ($filter_handlers = $display->getHandlers('filter')) {
        $filter_context['parents'] = $field_settings_parents;
        $filter_context['parents'][] = 'exposed_input';
        $filter_context['parents'][] = 'filter';
        $filter_context['handler_type'] = 'filter';

        foreach ($filter_handlers as $handler_id => $handler) {
          $handler_context = $filter_context;
          $handler_context['parents'][] = $handler_id;
          if ($handler->canExpose() && $handler->isExposed()) {
            $elements['exposed_input']['filter'][$handler_id] = $this->getHandlerFieldGroup(
              $handler->adminLabel(),
              NULL,
              $this->getNestedSetting(
                ['exposed_input', 'filter', $handler_id], []
              ),
              $handler_context
            );
          }
        }
      }

      // Build argument options.
      if ($argument_handlers = $display->getHandlers('argument')) {
        $argument_context['parents'] = $field_settings_parents;
        $argument_context['parents'][] = 'argument';
        $argument_context['handler_type'] = 'argument';
        $elements['argument'] = [
          '#type' => 'fieldset',
          '#title' => $this->t('Argument Filters'),
          '#weight' => 5,
        ];
        foreach ($argument_handlers as $handler_id => $handler) {
          $handler_context = $argument_context;
          $handler_context['parents'][] = $handler_id;
          $elements['argument'][$handler_id] = $this->getHandlerFieldGroup(
            $handler->adminLabel(),
            NULL,
            $this->getNestedSetting(['argument', $handler_id], []),
            $handler_context
          );
        }
      }
    }

    // Wrap element for AJAX replacement.
    $elements = [
      '#prefix' => '<div id="' . $html_wrapper_id . '">',
      '#suffix' => '</div>',
        // Pass the id along to other methods.
      '#wrapper_id' => $html_wrapper_id,
    ] + $elements;

    return $elements;
  }

  /**
   * Ajax callback to refresh the widget.
   */
  public static function settingsFormAjaxRefresh(array $form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $parents = $triggering_element['#array_parents'];
    array_pop($parents);
    return NestedArray::getValue($form, $parents);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $view_id = $this->getSetting('view_name');
    $display_id = $this->getSetting('display_id');

    if ($view_id && $view = $this->getView($view_id)) {
      $summary[] = t('View: @view', ['@view' => $view->label()]);

      if ($display = $this->getViewDisplay($view, $display_id)) {
        $summary[] = t('Display: @display', ['@display' => $display->display['display_title']]);
      }
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $cacheable_metadata = new CacheableMetadata();
    $view_mode = $this->getSetting('view_mode_backfill');
    $elements = parent::viewElements($items, $langcode);

    $parameters = [
      'enabled' => TRUE,
      'limit' => NULL,
      'argument' => [],
      'exposed_input' => [],
    ];

    foreach ($this->getAutomatedEntitiesToView($items, $parameters, $langcode, $cacheable_metadata) as $entity) {
      // Exit once we reach the limit.
      if ($parameters['limit'] > 0 && count(Element::children($elements)) >= $parameters['limit']) {
        break;
      }

      // Due to render caching and delayed calls, the viewElements() method
      // will be called later in the rendering process through a '#pre_render'
      // callback, so we need to generate a counter that takes into account
      // all the relevant information about this field and the referenced
      // entity that is being rendered.
      $recursive_render_id = $items->getFieldDefinition()->getTargetEntityTypeId()
        . $items->getFieldDefinition()->getTargetBundle()
        . $items->getName()
        // We include the referencing entity, so we can render default images
        // without hitting recursive protections.
        . $items->getEntity()->id()
        . $entity->getEntityTypeId()
        . $entity->id();

      if (isset(static::$recursiveRenderDepth[$recursive_render_id])) {
        static::$recursiveRenderDepth[$recursive_render_id]++;
      }
      else {
        static::$recursiveRenderDepth[$recursive_render_id] = 1;
      }

      // Protect ourselves from recursive rendering.
      if (static::$recursiveRenderDepth[$recursive_render_id] > static::RECURSIVE_RENDER_LIMIT) {
        $this->loggerFactory->get('entity')->error('Recursive rendering detected when rendering entity %entity_type: %entity_id, using the %field_name views backfill field on the %parent_entity_type:%parent_bundle %parent_entity_id entity. Aborting rendering.', [
          '%entity_type' => $entity->getEntityTypeId(),
          '%entity_id' => $entity->id(),
          '%field_name' => $items->getName(),
          '%parent_entity_type' => $items->getFieldDefinition()->getTargetEntityTypeId(),
          '%parent_bundle' => $items->getFieldDefinition()->getTargetBundle(),
          '%parent_entity_id' => $items->getEntity()->id(),
        ]);
        return $elements;
      }

      $view_builder = $this->entityTypeManager->getViewBuilder($entity->getEntityTypeId());
      $elements[] = $view_builder->view($entity, $view_mode, $entity->language()->getId());
    }

    return $elements;
  }

}
